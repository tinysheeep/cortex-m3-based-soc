#include "CortexM3_V10.h"

void NMIHandler(void) {
    ;
}

void HardFaultHandler(void) {
    ;
}

void MemManageHandler(void) {
    ;
}

void BusFaultHandler(void) {
    ;
}

void UsageFaultHandler(void) {
    ;
}

void SVCHandler(void) {
    ;
}

void DebugMonHandler(void) {
    ;
}

void PendSVHandler(void) {
    ;
}

void SysTickHandler(void) {
    ;
}


void UARTRXHandler(void) {
    ;
}

void UARTTXHandler(void) {
    ;
}

void UARTOVRHandler(void) {
    ;
}

void TimerHandler(void) {
    ;
}
