#include "CortexM3_V10.h"
#include "CortexM3_V10_driver.h"
#include "ov2640.h"
#include <stdio.h>

int main(void) {
    printf("start up!\r\n");
    GPIO_SetOutEnable(GPIO0, 0xFF03);

    char a[30];

    while(1) {
        GPIO0->DATAOUT |= 0xFF00;
        delay_ms(300);
        GPIO0->DATAOUT &= 0x00FF;
        delay_ms(300);
        if(OV2640_Init())
            printf("OV2640 Initialize failed\r\n");
        while(scanf("%s", a));
    }
}
