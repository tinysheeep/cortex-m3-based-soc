/******************************************************************************/
/* RETARGET.C: 'Retarget' layer for target-dependent low level functions      */
/******************************************************************************/
/* This file is part of the uVision/ARM development tools.                    */
/* Copyright (c) 2005 Keil Software. All rights reserved.                     */
/* This software may only be used under the terms of a valid, current,        */
/* end user licence from KEIL for a compatible version of KEIL software       */
/* development tools. Nothing else gives you the right to use this software.  */
/******************************************************************************/

#include <stdio.h>
#include "CortexM3_V10_driver.h"

//#pragma import(__use_no_semihosting_swi)

/*
If you want to use ARM C Library which is default,
then you must also redefine __FILE structure along with __stdout for fputc() and __stdin for fgetc() as given below:
struct __FILE { int handle; };
FILE __stdout;
FILE __stdin;
Note: Redefining __FILE , __stdout and __stdin is not required if you are using Microlib.
*/

int fputc(int ch, FILE *f) {
  if(ch == '\n') uart_SendChar(UART,'\r');
  uart_SendChar(UART,ch);
  return (ch);
}

int fgetc(FILE *f) {
  char buf;
  buf = uart_ReceiveChar(UART);
  uart_SendChar(UART,buf);
  if(buf == '\r') buf = '\n';
  return (buf);
}


int ferror(FILE *f) {
  /* Your implementation of ferror */
  return EOF;
}


void _ttywrch(int ch) {
  uart_SendChar(UART,ch);
}


void _sys_exit(int return_code) {
  while (1);    /* endless loop */
}
