#ifndef CORTEX_M3_DRIVER_H_
#define CORTEX_M3_DRIVER_H_

#include "CortexM3_V10.h"


/****************************UART*******************************/

extern uint32_t  uart_init( UART_TypeDef * uart, uint32_t divider, uint32_t tx_en,
                           uint32_t rx_en, uint32_t tx_irq_en, uint32_t rx_irq_en, uint32_t tx_ovrirq_en, uint32_t rx_ovrirq_en);

  /**
   * @brief Returns whether the UART RX Buffer is Full.
   */

 extern uint32_t  uart_GetRxBufferFull( UART_TypeDef * uart);

  /**
   * @brief Returns whether the UART TX Buffer is Full.
   */

 extern uint32_t  uart_GetTxBufferFull( UART_TypeDef * uart);

  /**
   * @brief Sends a character to the UART TX Buffer.
   */


 extern void  uart_SendChar( UART_TypeDef * uart, char txchar);

  /**
   * @brief Receives a character from the UART RX Buffer.
   */

 extern char  uart_ReceiveChar( UART_TypeDef * uart);

  /**
   * @brief Returns UART Overrun status.
   */

 extern uint32_t  uart_GetOverrunStatus( UART_TypeDef * uart);

  /**
   * @brief Clears UART Overrun status Returns new UART Overrun status.
   */

 extern uint32_t  uart_ClearOverrunStatus( UART_TypeDef * uart);

  /**
   * @brief Returns UART Baud rate Divider value.
   */

 extern uint32_t  uart_GetBaudDivider( UART_TypeDef * uart);

  /**
   * @brief Return UART TX Interrupt Status.
   */

 extern uint32_t  uart_GetTxIRQStatus( UART_TypeDef * uart);

  /**
   * @brief Return UART RX Interrupt Status.
   */

 extern uint32_t  uart_GetRxIRQStatus( UART_TypeDef * uart);

  /**
   * @brief Clear UART TX Interrupt request.
   */

 extern void  uart_ClearTxIRQ( UART_TypeDef * uart);

  /**
   * @brief Clear UART RX Interrupt request.
   */

 extern void  uart_ClearRxIRQ( UART_TypeDef * uart);

/****************************GPIO*******************************/

  /**
   * @brief Set GPIO Output Enable.
   */

 extern void GPIO_SetOutEnable(GPIO_TypeDef *GPIO, uint32_t outenableset);

  /**
   * @brief Clear GPIO Output Enable.
   */

 extern void GPIO_ClrOutEnable(GPIO_TypeDef *GPIO, uint32_t outenableclr);

  /**
   * @brief Returns GPIO Output Enable.
   */

 extern uint32_t GPIO_GetOutEnable(GPIO_TypeDef *GPIO);

  /**
   * @brief Set GPIO Alternate function Enable.
   */

 extern void GPIO_SetAltFunc(GPIO_TypeDef *GPIO, uint32_t AltFuncset);

  /**
   * @brief Clear GPIO Alternate function Enable.
   */

 extern void GPIO_ClrAltFunc(GPIO_TypeDef *GPIO, uint32_t AltFuncclr);

  /**
   * @brief Returns GPIO Alternate function Enable.
   */

 extern uint32_t GPIO_GetAltFunc(GPIO_TypeDef *GPIO);

  /**
   * @brief Clear GPIO Interrupt request.
   */

 extern uint32_t GPIO_IntClear(GPIO_TypeDef *GPIO, uint32_t Num);

  /**
   * @brief Enable GPIO Interrupt request.
   */

 extern uint32_t GPIO_SetIntEnable(GPIO_TypeDef *GPIO, uint32_t Num);

  /**
   * @brief Disable GPIO Interrupt request.
   */

 extern uint32_t GPIO_ClrIntEnable(GPIO_TypeDef *GPIO, uint32_t Num);

  /**
   * @brief Setup GPIO Interrupt as high level.
   */

 extern void GPIO_SetIntHighLevel(GPIO_TypeDef *GPIO, uint32_t Num);

   /**
   * @brief Setup GPIO Interrupt as rising edge.
   */

 extern void GPIO_SetIntRisingEdge(GPIO_TypeDef *GPIO, uint32_t Num);

     /**
   * @brief Setup GPIO Interrupt as low level.
   */

 extern void GPIO_SetIntLowLevel(GPIO_TypeDef *GPIO, uint32_t Num);

    /**
   * @brief Setup GPIO Interrupt as falling edge.
   */

 extern void GPIO_SetIntFallingEdge(GPIO_TypeDef *GPIO, uint32_t Num);

    /**
   * @brief Setup GPIO output value using Masked access.
   */

 extern void GPIO_MaskedWrite(GPIO_TypeDef *GPIO, uint32_t value, uint32_t mask);


 /**************************************SYSTICK*******************************************/

void Delay_Init(uint32_t SystemCoreClock);
void delay_us(uint32_t nus);
void delay_ms(uint32_t nms);

#endif
