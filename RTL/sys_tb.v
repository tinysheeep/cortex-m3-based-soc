module testbench ();
reg clk;
reg rstn;
wire TXD;
wire [15:0] GPIO;
wire SDA;
wire SCL;

cpu_top 
#(
    .is_Simulation (1 )
)
u_cpu_top(
    .CLK_50M (clk     ),
    .RST_n   (rstn    ),
    .SWDCLK  (1'b0    ),
    .SWDIO   (1'b0    ),
    .RXD     (1'b1    ),
    .TXD     (TXD     ),
    .GPIO    (GPIO    ),
    .SDA     (SDA     ),
    .SCL     (SCL     )
);


initial begin
    clk = 1;
    rstn = 0;
    #100 rstn = 1;
end

always #10 clk = ~clk;
    
endmodule