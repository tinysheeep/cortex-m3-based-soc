module ahb_slave_decoder(
    input       wire [15:0] HADDR_upper,
    output      wire        HSEL0,
    output      wire        HSEL1,
    output      wire        HSEL2,
    output      wire        HSEL3,
    output      wire        HSEL4,
    output      wire        HSEL5,
    output      wire        HSEL6,
    output      wire        HSEL7,
    output      wire        HSEL8,
    output      wire        HSEL9
);

reg [9:0] select;

always@(*) begin
    case (HADDR_upper)
        16'h2000 : select = 10'h200;
        16'h4000 : select = 10'h100;
        16'h4001 : select = 10'h080;
        default  : select = 10'h001;
    endcase
end

assign {HSEL0,HSEL1,HSEL2,HSEL3,HSEL4,
        HSEL5,HSEL6,HSEL7,HSEL8,HSEL9} = select;

endmodule