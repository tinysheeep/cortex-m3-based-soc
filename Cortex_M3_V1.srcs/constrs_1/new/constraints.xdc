set_property PACKAGE_PIN W19 [get_ports CLK_50M]
set_property IOSTANDARD LVCMOS33 [get_ports CLK_50M]


set_property IOSTANDARD LVCMOS33 [get_ports SWDCLK]
set_property PACKAGE_PIN D17 [get_ports SWDCLK]
set_property IOSTANDARD LVCMOS33 [get_ports SWDIO]
set_property PACKAGE_PIN C17 [get_ports SWDIO]
set_property IOSTANDARD LVCMOS33 [get_ports RST_n]
set_property PACKAGE_PIN AB21 [get_ports RST_n]


set_property IOSTANDARD LVCMOS33 [get_ports TXD]
set_property DRIVE 4 [get_ports TXD]
set_property PACKAGE_PIN Y22 [get_ports TXD]
set_property IOSTANDARD LVCMOS33 [get_ports RXD]
set_property PACKAGE_PIN Y21 [get_ports RXD]


# GPIO0
# OV2640 RST
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[0]}]
set_property PACKAGE_PIN U15 [get_ports {GPIO[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[1]}]
set_property PACKAGE_PIN AA9 [get_ports {GPIO[1]}]
# HEADERS
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[2]}]
set_property PACKAGE_PIN K4 [get_ports {GPIO[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[3]}]
set_property PACKAGE_PIN J4 [get_ports {GPIO[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[4]}]
set_property PACKAGE_PIN G3 [get_ports {GPIO[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[5]}]
set_property PACKAGE_PIN G4 [get_ports {GPIO[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[6]}]
set_property PACKAGE_PIN J5 [get_ports {GPIO[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[7]}]
set_property PACKAGE_PIN K2 [get_ports {GPIO[7]}]
# LED0-LED7
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[8]}]
set_property PACKAGE_PIN AA21 [get_ports {GPIO[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[9]}]
set_property PACKAGE_PIN AA20 [get_ports {GPIO[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[10]}]
set_property PACKAGE_PIN W22 [get_ports {GPIO[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[11]}]
set_property PACKAGE_PIN W21 [get_ports {GPIO[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[12]}]
set_property PACKAGE_PIN T20 [get_ports {GPIO[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[13]}]
set_property PACKAGE_PIN R19 [get_ports {GPIO[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[14]}]
set_property PACKAGE_PIN P19 [get_ports {GPIO[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO[15]}]
set_property PACKAGE_PIN U21 [get_ports {GPIO[15]}]


# I2C
# OV2640 SCL
set_property IOSTANDARD LVCMOS33 [get_ports SCL]
set_property PACKAGE_PIN Y12 [get_ports SCL]
set_property PULLUP true [get_ports SCL]
# OV2640 SDA
set_property IOSTANDARD LVCMOS33 [get_ports SDA]
set_property PACKAGE_PIN V14 [get_ports SDA]
set_property PULLUP true [get_ports SDA]

create_clock -period 100.000 -name SWDCLK -waveform {0.000 50.000} [get_ports SWDCLK]
