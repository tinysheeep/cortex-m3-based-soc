# Cortex-m3-based-SOC

#### 介绍
集创赛ARM杯比赛作品 (未完成)
Cortex-M3系统，包含一个UART外设，15个GPIO，Timer以及I2C

#### 系统架构
系统框图

![system diagram](https://gitee.com/tinysheeep/cortex-m3-based-soc/blob/master/cm3_diagram.png)

#### 使用说明

1.  RTL文件夹中包含系统所有RTL代码，代码含xilinx IP原语，可以自动推断出IP，还需添加PLL输出50MHz时钟至系统
2.  software文件夹中包含所有软件代码
3.  master分支含CPU
    VGA_test分支含CPU 摄像头接口模块 图像预处理模块 VGA模块
4.  参考极术社区集创赛Arm杯培训
